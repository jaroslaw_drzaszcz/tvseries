package TvSeries;
import java.time.LocalDate;


public class Episod {
	
	private String nameOfEpisod;
	private double lenghtOfEpisod;
	private int numberOfEpisog;
	private LocalDate dateOfFirstEmision;
	
	public Episod(String nameOfEpisod, double lenghtOfEpisod,
			int numberOfEpisog, LocalDate dateOfFirstEmision) {
		super();
		this.nameOfEpisod = nameOfEpisod;
		this.lenghtOfEpisod = lenghtOfEpisod;
		this.numberOfEpisog = numberOfEpisog;
		this.dateOfFirstEmision = dateOfFirstEmision;
	}

	public String getNameOfEpisod() {
		return nameOfEpisod;
	}

	public void setNameOfEpisod(String nameOfEpisod) {
		this.nameOfEpisod = nameOfEpisod;
	}

	public double getLenghtOfEpisod() {
		return lenghtOfEpisod;
	}

	public void setLenghtOfEpisod(double lenghtOfEpisod) {
		this.lenghtOfEpisod = lenghtOfEpisod;
	}

	public int getNumberOfEpisog() {
		return numberOfEpisog;
	}

	public void setNumberOfEpisog(int numberOfEpisog) {
		this.numberOfEpisog = numberOfEpisog;
	}

	public LocalDate getDateOfFirstEmision() {
		return dateOfFirstEmision;
	}

	public void setDateOfFirstEmision(LocalDate dateOfFirstEmision) {
		this.dateOfFirstEmision = dateOfFirstEmision;
	}

	

}
