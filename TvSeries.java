package TvSeries;

import java.util.ArrayList;

public class TvSeries {

 private String name;
 private ArrayList<Season> season;
 private int seasonNumber;
 private int yearOfFirstEmision;
 
 public TvSeries(String name, ArrayList<Season> season, int seasonNumber,
		int yearOfFirstEmision) {
	super();
	this.name = name;
	this.setSeason(season);
	this.seasonNumber = seasonNumber;
	this.yearOfFirstEmision = yearOfFirstEmision;
}
 
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getSeasonNumber() {
	return seasonNumber;
}
public void setSeasonNumber(int seasonNumber) {
	this.seasonNumber = seasonNumber;
}
public int getYearOfFirstEmision() {
	return yearOfFirstEmision;
}
public void setYearOfFirstEmision(int yearOfFirstEmision) {
	this.yearOfFirstEmision = yearOfFirstEmision;
}

public ArrayList<Season> getSeason() {
	return season;
}

public void setSeason(ArrayList<Season> season) {
	this.season = season;
}
 
}
