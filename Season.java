package TvSeries;

import java.util.ArrayList;

public class Season {
	
	 private String name;
	 private ArrayList<Episod> episod;
	 private int seasonNumber;
	 private int yearOfFirstEmision;
	
	 public Season(String name, ArrayList<Episod> episod, int seasonNumber,
			int yearOfFirstEmision) {
		super();
		this.name = name;
		this.episod = episod;
		this.seasonNumber = seasonNumber;
		this.yearOfFirstEmision = yearOfFirstEmision;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Episod> getEpisod() {
		return episod;
	}

	public void setEpisod(ArrayList<Episod> episod) {
		this.episod = episod;
	}

	public int getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public int getYearOfFirstEmision() {
		return yearOfFirstEmision;
	}

	public void setYearOfFirstEmision(int yearOfFirstEmision) {
		this.yearOfFirstEmision = yearOfFirstEmision;
	}
	

	 
}
